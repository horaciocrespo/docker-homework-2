package com.hcrespo.dockerhomework2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerHomework2Application {

    public static void main(String[] args) {
        SpringApplication.run(DockerHomework2Application.class, args);
    }

}
