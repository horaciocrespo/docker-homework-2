package com.hcrespo.dockerhomework2.controllers;

import com.hcrespo.dockerhomework2.model.ServerInfoDTO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.TimeZone;

@RestController
@RequestMapping(ServerInfoController.BASE_URL)
public class ServerInfoController {

    public static final String BASE_URL = "/api/serverInfo";

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ServerInfoDTO getTime() throws UnknownHostException {
        InetAddress ip = InetAddress.getLocalHost();
        return new ServerInfoDTO(
                LocalTime.now().toString(),
                LocalDate.now().toString(),
                TimeZone.getDefault().getDisplayName(),
                ip.getHostName(),
                ip.getHostAddress()
        );
    }

}

