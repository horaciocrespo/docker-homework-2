package com.hcrespo.dockerhomework2.model;

public class ServerInfoDTO {
    private String time;
    private String date;
    private String timezone;
    private String hostname;
    private String ip;

    public ServerInfoDTO(String time, String date, String timezone, String hostname, String ip) {
        this.time = time;
        this.date = date;
        this.timezone = timezone;
        this.hostname = hostname;
        this.ip = ip;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}

