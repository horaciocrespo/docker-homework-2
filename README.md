# Build image from source

**Clone the repository**

`git clone https://gitlab.com/horaciocrespo/docker-homework-2.git`

**Build the image**

`docker build -t docker-homework-2:horacio.crespo .`

**Run a container**

`docker run -p 8080:8080 docker-homework-2:horacio.crespo`

**Test your container**

Paste the following URL on the address bar of your favorite browser (Don't forget to replace the `<your-ip-address>` placeholder with your actual ip address).

`<your-ip-address>:8080/api/serverInfo`

You should get a json response similar to this one:

`{
    date: "2021-07-11",
    hostname: "eef94ca71984".
    ip: "172.17.0.2",
    time: "23:24:48.845",
    timezone: "Greenwich Mean Time",
}`

# Pull image from docker hub

https://hub.docker.com/repository/docker/horaciocrespo/docker-homework-2

# Start services using Docker Compose

Just run `docker compose-up` will will start the following services:

- App, run on port 8082
- Sonarqube, runs on port 9000
- Jenkins, runs on port 8080
- Nexus, runs on port 8081
- Database (Postgres), runs on port 5432
