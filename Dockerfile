# Install dependencies and package
FROM maven:alpine AS BUILD_IMAGE
ARG APP_DIR=/usr/src/myapp
COPY src ${APP_DIR}/src
COPY pom.xml ${APP_DIR}
WORKDIR ${APP_DIR}
RUN mvn clean package

# Copy Jar files
FROM openjdk:8-jdk-alpine AS JAR_IMAGE
ARG APP_DIR=/usr/src/myapp
COPY --from=BUILD_IMAGE ${APP_DIR}/target/*.jar ${APP_DIR}/app.jar
WORKDIR ${APP_DIR}
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/src/myapp/app.jar"]
